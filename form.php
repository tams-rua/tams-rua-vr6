<!DOCTYPE html>
<html>
<head>
  <title>Ajouter une Veille</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
  <link rel="stylesheet" type="text/css" href="style.css">
</head>

<script>
  $(document).ready(function(){
    $("#user_id").val(localStorage.getItem("id"));
  })
</script>

<body class="img-fluid" id="formulaire">

<div class="container">

<div class="modal-dialog text-center">
  <div class="col-lg-9 main-section">
    <div class="modal-content  bg-danger" id="modal-content">

      <div class="card-title pt-3">
       <h2 class="card-title text-light">Ajouter une Veille +</h2> 
      </div>
      
      <div class="col-12 form-input">
        <form enctype="multipart/form-data" action="php/formSend.php" method="POST">
        <div class="form-group">
        <input type="date" name="date" id="date" class="form-control">
        </div>
        <div class="form-group">
        <input type="text" class="form-control" name="sujet" id="sujet" placeholder="Sujet">
        </div>
        <div class="form-group">
        <input type="text" class="form-control" name="lien" id="lien" placeholder="URL">
        </div>
        <div class="mb-3">
        
        <div class="form-group">
          <input type="text" name="synthèse" id="synthèse" class="form-control" placeholder="Synthèse">
        </div>

        <div class="form-group">
          <input type="text" name="commentaire" id="commentaire" class="form-control" placeholder="Commentaire">
        </div>
        <div class="form-group" style="display:none;">
          <input type="number" name="user_id" id="user_id" class="form-control" placeholder="user_id">
        </div>
        <div class="form-group">
            <input type="hidden" name="MAX_FILE_SIZE" value="30000000" />
            <label for="loadimage">CHOISIR UNE IMAGE</label>
            <input type="file" id="loadimage" name="loadimage">

          </div>
          <input type="hidden" name="form" value="1">
          <input type="hidden" name="user_id" id="user_id">
          <button type="submit" class="btn btn-success mb-3">Enregistrer les informations</button>
        </form>
      
      </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>
</html>







