var Airtable = require('airtable');
Airtable.configure({
    endpointUrl: 'https://api.airtable.com',
    apiKey: 'key70l2I9iE1rMpX6'
});
var base = Airtable.base('appzwNchEdGvMM1Fx');

base('Veille').select({
    // Selecting the first 3 records in Tableau:
    view: "Tableau"
}).eachPage(function page(records, fetchNextPage) {
    // This function (`page`) will get called for each page of records.

    records.forEach(function (record) {
        console.log('Retrieved', record.get('Synthèse'));
        //document.getElementById('addVeille').innerHTML = document.getElementById('addVeille').innerHTML +  record.get('Synthèse');
        document.getElementById('list-veille').insertAdjacentHTML('afterbegin', ' <body class="thesaas-sections-split"></body> <section class="section"> <div class="container"> <header class="section-header"> <h2>' + record.get('Sujet') + '</h2> <hr> </header> <div class="row gap-y text-center"> <div class="col-12 col-md-6"> <blockquote class="blockquote mx-0"> <div><img class="avatar avatar-xl" src="assets/img/avatar/1.jpg" alt="...">Synthése</div><br><p>' + record.get('Synthèse') + '</p><footer>Steve Jobs</footer> </blockquote> </div></div></div><div class="container"> <header class="section-header"> <h2>COMMENTAIRE</h2> <hr> <p class="lead">' + record.get('Commentaire') + '</p><p class="lead">' + record.get('Liens') + '</p></header> </section> </body>');
    });

    // To fetch the next page of records, call `fetchNextPage`.
    // If there are more records, `page` will get called again.
    // If there are no more records, `done` will get called.
    fetchNextPage();

}, function done(err) {
    if (err) {
        console.error(err);
        return;
    }
});

// function showV(){
//     document.getElementById('createV').style.display='block';
//     document.getElementById('addV').style.display='none';
//     document.getElementById('feelV').style.display='block';
// }

// function maskV(){
//     document.getElementById('createV').style.display='none';
//     document.getElementById('addV').style.display='block';
//     document.getElementById('feelV').style.display='none';
//}