<?php 
    header('Content-Type: application/rss+xml');
    include "php/db_connection.php";
?>

<?php
//$today = date('Y-m-d H:i:s', strtotime('yesterday'));
$sql = "SELECT * FROM `veilles` ";
$sth = $bdd->prepare($sql);
$sth->execute();
$veilleRss = $sth->fetchAll(PDO::FETCH_ASSOC);
?>

<?php echo utf8_encode('<?xml version="1.0" encoding="UTF-8"?>');?>

<rss version="2.0">
    <channel>
        <title>Flux RSS my_veille</title>
        <description>my_veille flux RSS 2.0</description>
<?php
    for ($i=0; $i < sizeof($veilleRss); $i++) { 
?>
        <item>
            <image><?php echo utf8_encode($veilleRss[$i]['image']); ?></image>
            <title><?php echo utf8_encode($veilleRss[$i]["sujet"]); ?></title>
            <pubDate><?php echo utf8_encode($veilleRss[$i]["date"]); ?></pubDate>
            <description><?php echo utf8_encode($veilleRss[$i]["synthèse"]); ?></description>
            <description><?php echo utf8_encode($veilleRss[$i]["commentaire"]); ?></description>
            <link><?php echo utf8_encode($veilleRss[$i]["lien"]); ?></link>
        </item>
<?php
        }
?>
    </channel>
</rss>